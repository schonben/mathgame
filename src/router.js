import React from "react";
import {bindActionCreators} from "redux";
import {connect, Provider} from "react-redux";
import * as actionCreators from './actionCreators';
import {IndexRoute, Route, Router} from "react-router";
import store, { history } from "./store";

import App from "./components/App";
import Home from "./components/Home";
import NoRoute from "./components/NoRoute";

function mapStateToProps(state) {
    return { store: state };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators,dispatch);
}


const router = (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={connect(mapStateToProps,mapDispatchToProps)(App)}>
                <IndexRoute component={Home}/>
                <Route path="/test" component={Home}/>
                <Route path="/*" component={NoRoute}/>
            </Route>
        </Router>
    </Provider>
);
export default router;