import React from 'react';
import Component from "./Component";


export default class NoRoute extends Component {
    render () {
        return(
            <div>
                404 Route Not Found!
            </div>
        );
    }
}
