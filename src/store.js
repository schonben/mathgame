import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import { browserHistory } from "react-router";
import reducer from './reducers';
import { loadState, saveState } from './localStorage';
import throttle from "lodash/throttle";

const persistedState = loadState();
const router = routerMiddleware(browserHistory);
const store = createStore(reducer, persistedState, composeWithDevTools(applyMiddleware(router)));

store.subscribe(throttle(() => {
    saveState({ data: store.getState().data });
},1000));

export const history = syncHistoryWithStore(browserHistory, store);
export default store;