import React from 'react';
import { render } from 'react-dom';

import router from './router';

// Styles
import "../sass/style.scss";

render(
    router,
    document.getElementById('app')
);